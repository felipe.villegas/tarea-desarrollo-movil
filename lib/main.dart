import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.black,
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/background2.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                radius: 50.0,
                backgroundImage: AssetImage('images/felipe.png'),
              ),
              Text(
                'Felipe Villegas Sánchez',
                style: TextStyle(
                  height: 1.5,
                  fontSize: 24.0,
                  fontFamily: 'Eight-Bit-Dragon',
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                ),
              ),
              Text(
                'Estudiante UTFSM / Pixel Artist',
                style: TextStyle(
                  height: 2.0,
                  fontSize: 14.0,
                  fontFamily: 'Eight-Bit-Dragon',
                  color: Colors.white,
                ),
              ),
              Card(
                margin: EdgeInsets.symmetric(
                  vertical: 10.0,
                  horizontal: 25.0,
                ),
                child: ListTile(
                  leading: Icon(
                    Icons.call,
                    color: Colors.green[800],
                  ),
                  title: Text(
                    '+56 9 1234 1234',
                    style: TextStyle(
                      color: Colors.green[800],
                      fontSize: 18.0,
                    ),
                  ),
                  onTap: () => launch("tel:+56912341234"),
                ),
              ),
              Card(
                margin: EdgeInsets.symmetric(
                  vertical: 10.0,
                  horizontal: 25.0,
                ),
                child: ListTile(
                  leading: Icon(
                    Icons.email,
                    color: Colors.green[800],
                  ),
                  title: Text(
                    'felipe.villegas@usm.cl',
                    style: TextStyle(
                      color: Colors.green[800],
                      fontSize: 18.0,
                    ),
                  ),
                  onTap: () => launch("mailto:felipe.villegas@usm.cl?"),
                ),
              ),
              Card(
                margin: EdgeInsets.symmetric(
                  vertical: 10.0,
                  horizontal: 25.0,
                ),
                child: ListTile(
                  leading: Text(
                    'ROL',
                    style: TextStyle(
                      color: Colors.green[800],
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      height: 1.4,
                    ),
                  ),
                  onTap: () => launch("https://aula.usm.cl/"),
                  title: Text(
                    '202073038-5',
                    style: TextStyle(
                      color: Colors.green[800],
                      fontSize: 18.0,
                    ),
                  ),
                ),
              ),
              Card(
                margin: EdgeInsets.symmetric(
                  vertical: 10.0,
                  horizontal: 25.0,
                ),
                child: ListTile(
                  leading: Icon(
                    Icons.directions,
                    color: Colors.green[800],
                  ),
                  onTap: () => launch("https://goo.gl/maps/KaxuWiEmspR4JVT36"),
                  title: Text(
                    'Santiago 860, Villa Alemana',
                    style: TextStyle(
                      color: Colors.green[800],
                      fontSize: 18.0,
                    ),
                  ),
                ),
              ),
              Card(
                margin: EdgeInsets.symmetric(
                  vertical: 10.0,
                  horizontal: 25.0,
                ),
                child: ListTile(
                  leading: Icon(
                    Icons.videogame_asset,
                    color: Colors.green[800],
                  ),
                  onTap: () =>
                      launch("https://steamcommunity.com/id/nomikomu2208/"),
                  title: Text(
                    'Nomikomu',
                    style: TextStyle(
                      color: Colors.green[800],
                      fontSize: 18.0,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
